
import abc

import pyro
from pyro.contrib.autoname import scope
import pyro.distributions as dist


class ObservationModel(abc.ABC):

    @scope(prefix="likelihood")
    def likelihood_conditional(self, t, state, obs):
        return self._likelihood_conditional(t, state, obs)

    @abc.abstractmethod
    def _likelihood_conditional(self, t, state, obs):
        ...

    @scope(prefix="reward")
    def reward_conditional(self, t, obs):
        return self._reward_conditional(t, obs)

    @abc.abstractmethod
    def _reward_conditional(self, t, obs):
        ...
    
    @scope(prefix="observation")
    def observation_conditional(self, t, state, obs=None,):
        with scope(prefix=str(t)):
            obs = self.likelihood_conditional(t, state, obs)
            self.reward_conditional(t, obs)


class ActionModel(abc.ABC):
    
    @scope(prefix="action")
    def action_conditional(self, t, state):
        with scope(prefix=str(t)):
            return self._action_conditional(t, state)

    @abc.abstractmethod
    def _action_conditional(self, t, state):
        ...


class StateModel(abc.ABC):
    
    @scope(prefix="state")
    def state_conditional(self, t, last_action, last_state):
        with scope(prefix=str(t)):
            return self._state_conditional(t, last_action, last_state)

    @abc.abstractmethod
    def _state_conditional(self, t, last_action, last_state):
        ...

    @scope(prefix="state")
    def state_prior(self,):
        with scope(prefix="0"):
            return self._state_prior()

    @abc.abstractmethod
    def _state_prior(self,):
        ...


class ObservationGenerator(abc.ABC):

    @abc.abstractmethod
    def generate_observation(self, t, action):
        ...


class ActiveInferenceModel:

    def __init__(
        self,
        name,
        observation_model: ObservationModel,
        action_model: ActionModel,
        state_model: StateModel, 
        observation_generator: ObservationGenerator,
        num_timesteps=2,
    ):
        assert num_timesteps >= 2
        self._observation_model = observation_model
        self._action_model = action_model
        self._state_model = state_model
        self._observation_generator = observation_generator
        self._num_timesteps = num_timesteps
        self.__name__ = name

    def __call__(self, *args, **kwargs):
        state = self._state_model.state_prior()  # s_0
        obs = self._observation_generator.generate_observation(0, None)  # o_0
        self._observation_model.observation_conditional(0, state, obs=obs)
        action = self._action_model.action_conditional(0, state)  # a_0

        for t in pyro.poutine.markov(range(1, self._num_timesteps)):
            state = self._state_model.state_conditional(t, action, state)  # s_t
            obs = self._observation_generator.generate_observation(t, action)  #o_t
            self._observation_model.observation_conditional(t, state, obs=obs)  
            action = self._action_model.action_conditional(t, state)  # a_t
