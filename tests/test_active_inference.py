
import logging

import pyro
from pyro.contrib.autoname import scope
import pyro.distributions as dist
import torch

from ppl_kit.action import models
from ppl_kit.inference import metropolis, query


_invert = lambda d: {v: k for k, v in d.items()}

STATES = {"happy": 0, "sad": 1, "bored": 2}
OBSERVATIONS = {"Hello.": 2, "*staring*": 1, "Hi, how are you?": 0}
INVERSE_OBSERVATIONS = _invert(OBSERVATIONS)

REWARD = {0: 2, 1: -3, 2: 0}


class EmotionStateModel(models.StateModel):

    def _state_prior(self,):
        """
        A person can be happy, sad, or bored. 
        """
        return pyro.sample("emotion", dist.Categorical(torch.ones(3)/3))

    def _state_conditional(self, t, last_action, last_state):
        if last_action > 1:
            if last_state == STATES["happy"]:
                state_prob = torch.tensor([0.9, 0.05, 0.05])
            elif last_state == STATES["sad"]:
                state_prob = torch.tensor([0.5, 0.3, 0.2])
            else:
                state_prob = torch.tensor([0.3, 0.3, 0.4])
        elif last_action < -1:
            if last_state == STATES["happy"]:
                state_prob = torch.tensor([0.3, 0.5, 0.2])
            elif last_state == STATES["sad"]:
                state_prob = torch.tensor([0.05, 0.9, 0.05])
            else:
                state_prob = torch.tensor([0.3, 0.3, 0.4])
        else:
            state_prob = torch.tensor([0.3, 0.3, 0.4])
        return pyro.sample("emotion", dist.Categorical(state_prob))


class EmotionActionModel(models.ActionModel):

    def _action_conditional(self, t, state):
        if state == STATES["happy"]:
            d = dist.Normal(0.0, 0.5)
        elif state == STATES["sad"]:
            d = dist.Normal(1.0, 0.5)
        else:
            d = dist.Normal(0.0, 1.0)
        return pyro.sample("emotion", d)


class EmotionObservationModel(models.ObservationModel):

    def _likelihood_conditional(self, t, state, obs):
        if state == STATES["bored"]:
            state_prob = torch.tensor([0.1, 0.1, 0.8])
        elif state == STATES["sad"]:
            state_prob = torch.tensor([0.1, 0.8, 0.1])
        else:
            state_prob = torch.tensor([0.8, 0.1, 0.1])
        return pyro.sample("emotion",
            dist.Categorical(state_prob), obs=torch.tensor(OBSERVATIONS[obs]),)

    def _reward_conditional(self, t, obs):
        pyro.factor("emotion", torch.exp(torch.tensor(REWARD[int(obs)])))


class EmotionObservationGenerator(models.ObservationGenerator):

    def generate_observation(self, t, action):
        if action is None:
            action = 0.0
        if action > 1:
            return "Hi, how are you?"
        else:
            return INVERSE_OBSERVATIONS[int(torch.randint(3, (1,)))]


def test_emotion():

    sm = EmotionStateModel()
    trace = pyro.poutine.trace(sm.state_prior).get_trace()
    logging.info(trace.nodes)

    am = EmotionActionModel()
    trace = pyro.poutine.trace(am.action_conditional).get_trace(1, 1)
    logging.info(trace.nodes)

    om = EmotionObservationModel()
    trace = pyro.poutine.trace(om.observation_conditional).get_trace(1, 1, "Hello.")
    logging.info(trace.nodes)

    obs_gen = EmotionObservationGenerator()
    logging.info(obs_gen.generate_observation(1, 3.0))
    for _ in range(5):
        logging.info(obs_gen.generate_observation(_, 0.0))

    aim = models.ActiveInferenceModel(
        "emotion_aim",
        om,
        am,
        sm,
        obs_gen,
        3,
    )
    trace = pyro.poutine.trace(aim).get_trace()
    logging.info(trace.nodes)

    NS = 100

    mh = metropolis.SingleSiteMH(
        aim,
        samples=NS,
        lag=100,
        burn=1000,
    )
    mh = mh.run()
    rmc = query.RelationalModelCollection((mh,), num_samples=2 * NS)
    rmc.create_db(predictive=True)
    results = rmc.query_db(
        """
        select
            observation__0__likelihood__emotion as obs_0,
            action__0__emotion as a0,
            observation__1__likelihood__emotion as obs_1,
            action__1__emotion as a1,
            observation__2__likelihood__emotion as obs_2,
            action__2__emotion as a2,
            total_loglikelihood as value
        from emotion_aim
        where
            -- someone is sad at least once
            observation__0__likelihood__emotion = 1 or
            observation__1__likelihood__emotion = 1 or
            observation__2__likelihood__emotion = 1
        order by total_loglikelihood desc;
        """
    )
    logging.info(results)
