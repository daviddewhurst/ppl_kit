from setuptools import setup, find_packages


setup(
    name="ppl_kit",
    version="0.1.0",
    packages=find_packages(),
)
